/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wittaya.shapepro1;

/**
 *
 * @author AdMiN
 */
public class Square {
     private double w;
    public Square(double w){
        this.w = w;
    }
    public double calArea(){
        return w*w;
    }
    public void setS(double w){
        if(w <= 0){
            System.out.println("Error: Length must be more than zero");
            return;
        }
        this.w = w;
    }
    public String getS(){
        return "(W = " + w + ")";
    }
}
